# SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
# SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
#
# SPDX-License-Identifier: Apache-2.0

QT = core

#VERSION = "symm-comp"
#VERSION = "rms-ac-dc"
#VERSION = "ip-dft"
VERSION = "raw-corrected"

TARGET = dsp_$${VERSION}
DEFINES += DSP_LIB=\"\\\"libs/lib_$${VERSION}/lib_$${VERSION}.h\\\"\" \
           DSP_NAME=\"\\\"dsp_$${VERSION}\\\"\"

TEMPLATE = lib

DEFINES += SMUDSP_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \ 
    smudsp.cpp

HEADERS += \
    smudsp.h \
    smudsp_global.h

include(../smuLIB/smuLIB.pri)
include(libs/lib_$${VERSION}/lib_$${VERSION}.pri)
message("Building shared lib: dsp_$${VERSION}.so")

unix {
    target.path = /usr/lib/smu
    INSTALLS += target
}

QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3
QMAKE_LFLAGS_RELEASE -= -O1


BUILDDIR = ./build
DESTDIR = ../build/libs
OBJECTS_DIR = $$BUILDDIR
MOC_DIR = $$BUILDDIR
RCC_DIR = $$BUILDDIR
