// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "lib_raw-corrected.h"
#include <math.h>

static subCFG *smuCFG;
static subMEM *smuMEM;

static smu_mcsc_t *mcsc; 
static smu_mcsr_t *data;
static uint32_t ns, nc, nr, Ns, Nc, Nr;

/// ---------- SMU: DSP virtual methods section ----------

/**
 * @brief DSP allocate memory
 *
 */
int lib_allocate(subCFG *cfg, subMEM *mem)
{
    smuCFG = cfg;
    smuMEM = mem;

    Ns = smuCFG->daq.rate*1000;                         // Sampling rate
    Nr = smuCFG->dsp.rate;                              // Reporting rate
    Nc = Ns/Nr;                                         // MCSamples per frame

    if (Nr>Ns)
        return -1; // Invalid reporting rate

    if (Ns % Nr)
        return -2; // Invalid rate ratio

    // Allocate data buffer
    smuMEM->dsp_pos = 0;
    smuMEM->dsp_inc = Nc*sizeof(smu_mcsr_t);            // Size of dsp_data_t
    smuMEM->dsp_data= (quint8*) calloc(Ns, sizeof(smu_mcsr_t));
    smuMEM->dsp_dim = Nc;

    // Link buffers
    qDebug()<<"sizeof(smu_mcsc_t)"<<sizeof(smu_mcsc_t);
    mcsc = (smu_mcsc_t*)smuMEM->daq_data;
    data = (smu_mcsr_t*)smuMEM->dsp_data;


    // Init indexes
    nr = 0;
    nc = 0;

    return 0;
}

/**
 * @brief DSP destroy memory
 *
 */
void lib_destroy()
{
    free(data);
}

/**
 * @brief DSP process multi-channel sample code
 *
 */
int  lib_process()
{
    // Reset cyclic indexes
    ns  = smuMEM->daq_pos;
    nr %= Nr;
    nc %= Nc;
    // Convert and push new value
    subMEM::mcs_convert(&mcsc[ns], &data[ns], &smuCFG->p1);
    nc++;

    // Skip to next iteration
    if (nc<Nc)
        return ((int)(nc-Nc));

    // Process output data
    smuMEM->dsp_pos = nr;
    return (nr++);
}

