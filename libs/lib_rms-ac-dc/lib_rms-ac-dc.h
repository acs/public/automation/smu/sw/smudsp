// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#ifndef LIB_RMS_AC_DC_H
#define LIB_RMS_AC_DC_H

#include <subtypes.h>
#include <subcfg.h>
#include <submem.h>

int  lib_allocate(subCFG *cfg, subMEM *mem);
void lib_destroy();
int  lib_process();
void lib_log(FILE *fid);

#endif // LIB_RMS_AC_DC_H
