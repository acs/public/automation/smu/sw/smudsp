// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "lib_rms-ac-dc.h"
#include <math.h>

static subCFG *smuCFG;
static subMEM *smuMEM;

static smu_mcsc_t *mcsc; 
static syn_meas_t *data;
static uint32_t ns, nc, nr, Ns, Nc, Nr, Tw;


static int64_t sqr[smu_Nch];
static int32_t avg[smu_Nch];

/// ---------- SMU: DSP virtual methods section ----------

/**
 * @brief DSP allocate memory
 *
 */
int lib_allocate(subCFG *cfg, subMEM *mem)
{
    smuCFG = cfg;
    smuMEM = mem;

    Ns = smuCFG->daq.rate*1000;                         // Sampling rate
    Nr = smuCFG->dsp.rate;                              // Reporting rate
    Nc = Ns/Nr;                                         // MCSamples per frame
    Tw = 1000/Nr;                                       // Sampling rate period in Miliseconds for the frame window

    if (Nr>Ns)
        return -1; // Invalid reporting rate

    if (Ns % Nr)
        return -2; // Invalid rate ratio

    // Allocate data buffer
    smuMEM->dsp_pos = 0;
    smuMEM->dsp_inc = sizeof(syn_meas_t);               // Size of dsp_phas_t
    smuMEM->dsp_data= (quint8*) calloc(Nr, Nr*smuMEM->dsp_inc);

    // Link buffers
    mcsc = (smu_mcsc_t*)smuMEM->daq_data;
    data = (syn_meas_t*)smuMEM->dsp_data;

    // Init indexes
    nr = 0;
    nc = 0;

    return 0;
}

/**
 * @brief DSP destroy memory
 *
 */
void lib_destroy()
{
    free(data);
}

/**
 * @brief DSP process multi-channel sample code
 *
 */
int  lib_process()
{
    // Reset cyclic indexes
    ns  = smuMEM->daq_pos;
    nr %= Nr;
    nc %= Nc;

    for (int i = 0; i<smu_Nch; i++)
    {
        sqr[i] = sqr[i] +  ((int64_t)mcsc[ns].ch[i])*((int64_t)mcsc[ns].ch[i]);
        avg[i] = avg[i] +  ((int32_t)mcsc[ns].ch[i]);
    }

    //Increase counter
    nc++;

    // Skip to next iteration
    if (nc<Nc)
        return ((int)(nc-Nc));

    // Process output data
    if (smuCFG->daq.mode == 2)
        data[nr].t_stamp = (QDateTime::currentMSecsSinceEpoch());
    else
        data[nr].t_stamp = smuMEM->tref.addMSecs(((nr+1)%Nr)*Tw+(Tw/2)).toMSecsSinceEpoch();
    
    // Convert and push new value
    for (int i = 0; i<smu_Nch; i++)
    {
        data[nr].rms.ch[i]  = (float) sqrt(((float)(double)sqr[i]/(double)Nc));
        data[nr].dc.ch[i]   = (float) avg[i]/(float)Nc;
    }

    for (int ch=0; ch<smu_Nch; ch++)
    {
        if ((nr+1)==Nr)
        {
            //instrument frequency drift compensation
            data[nr].rms.ch[ch] = smuCFG->p1.f_drift.gain*data[nr].rms.ch[ch] +
                                  smuCFG->p1.f_drift.offset;
            data[nr].dc.ch[ch] =  smuCFG->p2.f_drift.gain*data[nr].dc.ch[ch] +
                                  smuCFG->p2.f_drift.offset;          
        }

        //isntrument gain and offset compensation
        data[nr].rms.ch[ch] = smuCFG->p1.ch[ch].gain*data[nr].rms.ch[ch] +
                              smuCFG->p1.ch[ch].offset;
        data[nr].dc.ch[ch] =  smuCFG->p2.ch[ch].gain*data[nr].dc.ch[ch] +
                              smuCFG->p2.ch[ch].offset;
        
        data[nr].ac.ch[ch]   = (float) sqrt(data[nr].rms.ch[ch]*data[nr].rms.ch[ch]-data[nr].dc.ch[ch]*data[nr].dc.ch[ch]);
        if (isnan(data[nr].ac.ch[ch]))
            data[nr].ac.ch[ch]=0;

        if (data[nr].rms.ch[ch] < 0)
            {
                data[nr].rms.ch[ch]=0;
                data[nr].dc.ch[ch] =0;
                data[nr].ac.ch[ch] =0;
            }
    }

    for (int i = 0; i<smu_Nch; i++)
    {
        sqr[i] = 0;
        avg[i] = 0;
    }
    smuMEM->dsp_pos = nr;
    return (nr++);
}

