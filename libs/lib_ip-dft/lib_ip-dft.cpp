// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//Based on "Iterative-interpolated DFT for synchrophasor estimation in M-class compliant PMUs"
//Source: https://ieeexplore.ieee.org/document/7980868
// SPDX-License-Identifier: Apache-2.0

#include "lib_ip-dft.h"
#include <math.h>
#include <string.h>
#include <cmath>
#include <complex>
#include <iostream>
#include <vector>
#include <iostream>
#include <fstream>

#define cabs(R,I) sqrt(R*R + I*I)
#define PI 3.141592653589793238462643383279502884L
#define kn 16                                          // Number of frequency bins                            

using namespace std;

static int Kn;

int Km;

static subCFG *smuCFG;
static subMEM *smuMEM;

/**
 * @brief Code to voltage coefficients
 *
 */
static smu_mcsk_t mcsk = {
    .k={10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0}
};

static smu_mcsc_t *mcsc;
static dsp_phas_t *data;
static double data_double;
static uint32_t ch, ns, nc, nr, Ns, Nc, Nr, Tw;

static int8_t   epsil;
static double   alpha;
static double   delta;
static double   f0 = 50.0F;                             // Nominal frequency

//edit_
double b;
double B;
double Kmin;
double Kmax;
double dw;
// our dft
static double Xkr[smu_Nch][kn];
static double Xki[smu_Nch][kn];
static double Xka[smu_Nch][kn];

// windowing in frequency domain

static double wXka[smu_Nch][kn];
static double wXkr[smu_Nch][kn];
static double wXki[smu_Nch][kn];
double inputSignal;

//twiddle factor for truncated DFT
static double twf_dft_r[10000][kn];
static double twf_dft_i[10000][kn];

static smu_mcsr_t last_f;

//double wrap(double x) {return (fmod(x+PI,2*PI)-PI);}
/// ---------- SMU: DSP virtual methods section ----------

/**
 * @brief DSP allocate memory
 *
 */

int lib_allocate(subCFG *cfg, subMEM *mem)
{
    smuCFG = cfg;
    smuMEM = mem;

    Ns = smuCFG->daq.rate*1000;                         // Sampling rate
    Nr = smuCFG->dsp.rate;                              // Reporting rate
    Nc = Ns/Nr;                                         // MCSamples per frame
    Tw = 1000/Nr;                                       // Time window per frame (ms)
    if (Nr < 5)
        Kn = 16;
    else
        Kn = f0/Nr+2; //Number of frequency bins given the frequency resolution: Fres=Nr=Fs/Nc

    if ((Kn%2)==1)
        Kn=Kn+1;
    //Edit_
    Kmin = (f0 / Nr) - (Kn / 2);
    Kmax = Kmin + Kn;
    //Hanning window Definition
    B = 0.5 * Nc; //Integration of wh over the whole window
    b = 1 / B;
    dw = 2 * PI / Nc; //fraction of omega increased per processed DFT sample

    
    if (smuCFG->p1.ch[0].gain!=0)
        for (ch=0; ch<smu_Nch; ch++)
            mcsk.k[ch] = smuCFG->p1.ch[ch].gain; //m number of channels
    
    if (Nr>Ns)
        return -1; // Invalid reporting rate

    if (Ns % Nr)
        return -2; // Invalid rate ratio
    
    // Allocate data buffer
    smuMEM->dsp_pos = 0;
    smuMEM->dsp_inc = sizeof(dsp_phas_t);               // Size of dsp_phas_t
    smuMEM->dsp_data= (quint8*) calloc(Nr, Nr*smuMEM->dsp_inc);

    // Link buffers
    mcsc = (smu_mcsc_t*)smuMEM->daq_data;
    data = (dsp_phas_t*)smuMEM->dsp_data;

    
    //twiddle factor for DFT
    for (nc = 0; nc < Nc ;nc++)
    {
        for (int k = 0; k < Kn; k++)
        {
                twf_dft_r[nc][k] =  cos(((double)(nc*(k+Kmin+1)))*dw);
                twf_dft_i[nc][k] = -sin(((double)(nc*(k+Kmin+1)))*dw);
        }
    }

    // Init indexes
    nr = Nr-1;
    nc = 0; 
    // nc = Nc/2;

    return 0;
}

    /**
     * @brief DSP destroy memory
     *
     */
    void lib_destroy()
    {
        free(data);
    }

/**
 * @brief DSP process multi-channel sample code
 *
 */
int  lib_process()
{
    int k;

    // Reset cyclic indexes
    ns  = smuMEM->daq_pos;
    nr = (nr%Nr);
    nc = (nc%Nc);
    // Process output data
    for (ch=0; ch<smu_Nch; ch++)
    {
        //converts ADC data into double
        data_double = (double)(((float)(mcsc[ns].ch[ch]))*smuCFG->p1.ch[ch].gain+smuCFG->p1.ch[ch].offset);    
    
        // Truncated IpDFT
        for(k = 0; k < Kn; k++)
        {
            Xki[ch][k] = Xki[ch][k] + data_double * twf_dft_i[nc][k];

            Xkr[ch][k] = Xkr[ch][k] + data_double * twf_dft_r[nc][k];
        }  

    }
    nc++;
    
    if (nc<Nc)
        return -1;
    
    for (ch=0; ch<smu_Nch; ch++)
    {
        Km=0;
        for (k = 0; k < Kn; k++)
        {
            Xkr[ch][k] = Xkr[ch][k] * b;
            Xki[ch][k] = Xki[ch][k] * b;
            Xka[ch][k] = cabs(Xkr[ch][k],Xki[ch][k]);

            if(Xka[ch][k] > Xka[ch][Km])
            {
                Km = k;
            }
        }
        
        for (k = 1; k < (Kn - 1); k++)
        {
            wXkr[ch][k] = -0.25 * Xkr[ch][k - 1] + 0.5 * Xkr[ch][k] - 0.25 * Xkr[ch][k + 1];
            wXki[ch][k] = -0.25 * Xki[ch][k - 1] + 0.5 * Xki[ch][k] - 0.25 * Xki[ch][k + 1];
            wXka[ch][k] = cabs(wXkr[ch][k],wXki[ch][k]);
        }
        
        for (k = 0; k < Kn; k++)
        {
            Xkr[ch][k] = wXkr[ch][k];
            Xki[ch][k] = wXki[ch][k];
            Xka[ch][k] = wXka[ch][k];
        }

        // Interpolated DFT (Ip-DFT)

        if (Xka[ch][Km+1] > Xka[ch][Km-1])
            epsil = 1;
        else
            epsil = -1;

        alpha = abs(Xka[ch][Km] / Xka[ch][Km+epsil]);

        delta = ((double)epsil) * (2 - alpha) / (1 + alpha);

        if (abs(delta) < 0.000001)
            data[nr].A.ch[ch] = (float)(2*Xka[ch][Km]);
        else
            data[nr].A.ch[ch] = (float)(2*Xka[ch][Km]*(PI*delta*(1-delta*delta))/sin(PI*delta));
        
        
        data[nr].f.ch[ch] = (float)(((double)(Km + Kmin + 1) + delta)*((double)Nr));
        
        data[nr].P.ch[ch] = (float)(atan2(Xki[ch][Km],Xkr[ch][Km]) + PI*delta);
        

        for (k = 0; k < Kn; k++)
        {
            Xkr[ch][k]=0;
            Xki[ch][k]=0;
            wXkr[ch][k]=0;
            wXki[ch][k]=0;
        }

        //checking for validity of Amplitude value
        if (isnan(data[nr].A.ch[ch]))
            data[nr].A.ch[ch]=0;

        //checking for validity of Phase value
        if (isnan(data[nr].P.ch[ch]))
            data[nr].P.ch[ch]=0;

        //checking for validity of Frequency value
        if (isnan(data[nr].f.ch[ch]))
            data[nr].f.ch[ch]=0;

    //instrument error compensation process
        if ((nr+1)==Nr)
        {
            //instrument frequency drift compensation
            
            data[nr].P.ch[ch] = smuCFG->p2.f_drift.gain*data[nr].P.ch[ch] +
                                ((smuCFG->p3.f_drift.gain*data[nr].f.ch[ch]+smuCFG->p3.f_drift.offset)-data[nr].f.ch[ch])*.2 +
                                smuCFG->p2.f_drift.offset;
            data[nr].f.ch[ch] = smuCFG->p3.f_drift.gain*data[nr].f.ch[ch] + 
                                smuCFG->p3.f_drift.offset;
            data[nr].A.ch[ch] = smuCFG->p1.f_drift.gain*data[nr].A.ch[ch] +
                                smuCFG->p1.f_drift.offset;
            data[nr].df.ch[ch]= smuCFG->p4.f_drift.gain*data[nr].df.ch[ch]+
                                smuCFG->p4.f_drift.offset;
            
        }
        //isntrument gain and offset compensation
        data[nr].P.ch[ch] = smuCFG->p2.ch[ch].gain*data[nr].P.ch[ch] +
                            smuCFG->p2.ch[ch].offset;
        data[nr].f.ch[ch] = smuCFG->p3.ch[ch].gain*data[nr].f.ch[ch] + 
                            smuCFG->p3.ch[ch].offset;
        
        //ROCOF calculation
        data[nr].df.ch[ch]=(data[nr].f.ch[ch]-last_f.ch[ch])*Nr;
        last_f.ch[ch]=data[nr].f.ch[ch];

        data[nr].df.ch[ch]= smuCFG->p4.ch[ch].gain*data[nr].df.ch[ch]+ 
                            smuCFG->p4.ch[ch].offset;
        
    }

    // Process output data
    // data[nr].t_stamp = smuMEM->tref.addMSecs(((nr+1)%Nr)*Tw+(Tw/2)).toMSecsSinceEpoch(); 
    if (smuCFG->daq.mode == 2)
        data[nr].t_stamp = (QDateTime::currentMSecsSinceEpoch());
    else
        data[nr].t_stamp = smuMEM->tref.addMSecs(((nr+1)%Nr)*Tw).toMSecsSinceEpoch(); //Phase at the beginning of the Window
        // data[nr].t_stamp = smuMEM->tref.addMSecs(((nr+1)%Nr-Nr)*Tw+(Tw/2)).toMSecsSinceEpoch(); //Phase at the middle of the Window;
    smuMEM->dsp_pos = nr;

    return (nr++);
}

/**
 * @brief DSP log output data
 *
 */
void lib_log(FILE *fid)
{
    uint32_t n = (smuMEM->dsp_pos+1)%Nr;
    fprintf(fid,"%10.7f %10.7f %10.7f\n",data[n].A.ch[0],data[n].P.ch[0],data[n].f.ch[0]);
}