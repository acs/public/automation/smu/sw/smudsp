// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "lib_wave.h"
//#include "smudsp.h"
#include <math.h>

static subCFG *smuCFG;
static subMEM *smuMEM;

/**
 * @brief Code to voltage coefficients
 *
 */
static smu_mcsk_t mcsk = {
    .k={10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0}
};

/**
 * @brief Reporting frame struct
 *
 */
typedef struct {
    qint64 t_stamp;
    smu_mcsr_t *mcsr;
} __attribute__((__packed__)) dsp_data_t;

static dsp_data_t *data;
static smu_mcsr_t *fifo;
static uint32_t nr, nc, Nc, Ns, Nr, Tw;

static uint16_t m = 0;                                         // Cyclic buffer index
static uint16_t M = 600;                                       // Cyclic buffer size


/// ---------- SMU: DSP virtual methods section ----------

/**
 * @brief DSP allocate memory
 *
 */
int lib_allocate(subCFG *cfg, subMEM *mem)
{
    smuCFG = cfg;
    smuMEM = mem;

    Ns = smuCFG->daq.rate*1000;                         // Sampling rate
    Nr = smuCFG->dsp.rate;                              // Reporting rate
    Nc = Ns/Nr;                                         // MCSamples per frame
    Tw = 1000/Nr;                                       // Time window per frame (ms)

    if (Nr>Ns)
        return -1; // Invalid reporting rate

    if (Ns % Nr)
        return -2; // Invalid rate ratio

    // Temporary buffers
    fifo = (smu_mcsr_t*) calloc(M, M*sizeof(smu_mcsr_t));

    // Output data buffer
    smuMEM->dsp_pos = 0;
    smuMEM->dsp_inc = sizeof(qint64) + Nc*sizeof(smu_mcsr_t);               // Size of dsp_data_t
    smuMEM->dsp_data= (quint8*) calloc(Nr, Nr*smuMEM->dsp_inc);
    data = (dsp_data_t*)smuMEM->dsp_data;
    for(nr=0; nr<Nr; nr++)
        data[nr].mcsr = (smu_mcsr_t*) calloc(Nc, Nc*sizeof(smu_mcsr_t));

    // Init indexes
    m  = 0;
    nr = 0;
    nc = 0;

    return 0;
}

/**
 * @brief DSP destroy memory
 *
 */
void lib_destroy()
{
    for(nr=0; nr<Nr; nr++)
        free(data[nr].mcsr);
    free(data);
    free(fifo);
}

/**
 * @brief DSP process multi-channel sample code
 *
 */
int  lib_process()
{
    // Reset cyclic indexes
    nr = (nr%Nr);
    nc = (nc%Nc);
    m  = (m % M);

    // Convert and push new value
    subMEM::mcs_convert(&mcsc[offset], &fifo[m], &mcsk);

    // Fill output buffer
    memcpy(&data[nr].mcsr[nc], &fifo[m], sizeof(smu_mcsr_t));
    nc++; m++;

    // Skip to next iteration
    if (nc<Nc)
        return -1;

    // Process output data
    data[nr].t_stamp = smuMEM->tref.addMSecs(nr*Tw).toMSecsSinceEpoch();
    smuMEM->dsp_pos = nr;

    return (nr++);
}

/**
 * @brief DSP log output data
 *
 */
void lib_log(FILE *fid)
{

}
