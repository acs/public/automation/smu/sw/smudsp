// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "lib_symm-comp.h"
#include <math.h>

#define cabs(R,I) sqrt(R*R + I*I)
#define PI 3.141592653589793238462643383279502884L
#define Kn 5                                            // Number of frequency bins
#define Km 2                                            // Index of the DFT maximum (3rd bin)

static subCFG *smuCFG;
static subMEM *smuMEM;

/**
 * @brief Code to voltage coefficients
 *
 */
static smu_mcsk_t mcsk = {
    .k={10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0,
        10.0/32768.0}
};

static smu_mcsc_t *mcsc;
static dsp_phas_t *phasor;
static smu_mcsr_t *fifo, old;
static uint32_t ch, ns, nc, nr, Ns, Nc, Nr, Tw;
static quint8   *dsp_data;

static int8_t   epsil;
static double   alpha;
static double   delta;
static double   f0 = 50.0F;                             // Nominal frequency
static uint16_t k0;                                     // Polyphase demodulation index
static uint16_t k1;                                     // Polyphase modulation index
static uint16_t K;                                      // Frequency bin index
static uint16_t m;                                      // Cyclic buffer index
static uint16_t M;                                      // Cyclic buffer size

static double *wMr,*wMi,dx;
static double dX0r[Kn];
static double dX0i[Kn];
static double mX0r[smu_Nch][Kn];
static double mX0i[smu_Nch][Kn];
static double mXkr[smu_Nch][Kn];
static double mXki[smu_Nch][Kn];
static double mXhr[Kn];
static double mXhi[Kn];
static double mXhA[Kn];

static  symmetrical_volt_curr_t *sym_v_i;
static  complex_rectangular_t sym_calculation_r[3][3];
static  complex_rectangular_t component_calculation_r;
static  complex_polar_t sym_calculation_p[3][3];
static  complex_polar_t component_calculation_p;


static const   complex_polar_t inv_A[3][3] = 
    {{{(float)1/3,(float)0},           {(float)1/3,(float)0},                {(float)1/3,(float)0}},
     {{(float)1/3,(float)0},           {(float)1/3,(float)(2*PI/3)},         {(float)1/3,(float)(4*PI/3)}},
     {{(float)1/3,(float)0},           {(float)1/3,(float)(4*PI/3)},         {(float)1/3,(float)(2*PI/3)}}};

double wrap(double x) {return (fmod(x+PI,2*PI)-PI);}
/// ---------- SMU: DSP virtual methods section ----------

/**
 * @brief DSP allocate memory
 *
 */
int lib_allocate(subCFG *cfg, subMEM *mem)
{
    smuCFG = cfg;
    smuMEM = mem;

    Ns = smuCFG->daq.rate*1000;                         // Sampling rate
    Nr = smuCFG->dsp.rate;                              // Reporting rate
    Nc = Ns/Nr;                                         // MCSamples per frame
    M  = 3*Ns/f0;                                       // Cyclic buffer size
    Tw = 1000/Nr;                                       // Time window per frame (ms)

    if (smuCFG->p1.ch[0].gain!=0)
        for (m=0; m<8; m++)
            mcsk.k[m] = smuCFG->p1.ch[m].gain;
    
    if (Nr>Ns)
        return -1; // Invalid reporting rate

    if (Ns % Nr)
        return -2; // Invalid rate ratio

    // Temporary buffers
    fifo = (smu_mcsr_t*) calloc(M, M*sizeof(smu_mcsr_t));

    // Allocate phasor buffer
    dsp_data= (quint8*) calloc(Nr, Nr*sizeof(dsp_phas_t));
    
    // Allocate output buffer
    smuMEM->dsp_pos = 0;
    smuMEM->dsp_inc = sizeof(symmetrical_volt_curr_t);
    smuMEM->dsp_data= (quint8*) calloc(Nr, Nr*sizeof(symmetrical_volt_curr_t));

    // Link buffers
    phasor = (dsp_phas_t*) dsp_data;
    mcsc = (smu_mcsc_t*)smuMEM->daq_data;
    sym_v_i = (symmetrical_volt_curr_t*)smuMEM->dsp_data;


    // Modulation twiddle
    wMr = (double*) calloc(M,sizeof(double));
    wMi = (double*) calloc(M,sizeof(double));
    for(m=0;m<M;m++)
    {
        wMr[m] = cos(-2*PI*m/M);
        wMi[m] = sin(-2*PI*m/M);
    }
    memset(dX0r, 0.0L, Kn*sizeof(double));
    memset(dX0i, 0.0L, Kn*sizeof(double));
//    memset(mX0r, 0.0L, Kn*sizeof(double));
//    memset(mX0i, 0.0L, Kn*sizeof(double));
//    memset(mXkr, 0.0L, Kn*sizeof(double));
//    memset(mXki, 0.0L, Kn*sizeof(double));
    memset(mXhr, 0.0L, Kn*sizeof(double));
    memset(mXhi, 0.0L, Kn*sizeof(double));
    memset(mXhA, 0.0L, Kn*sizeof(double));

    // Init indexes
    m  = 0;
    nr = Nr-1;
    nc = Nc-M/2;                                        // Timestamp compensation delay

    return 0;
}

/**
 * @brief DSP destroy memory
 *
 */
void lib_destroy()
{
    free(wMr);
    free(wMi);
    free(dsp_data);
    free(phasor);
    free(fifo);
}

/**
 * @brief DSP process multi-channel sample code
 *
 */
int  lib_process()
{
    // Reset cyclic indexes
    ns  = smuMEM->daq_pos;
    nr = (nr%Nr);
    nc = (nc%Nc);
    m  = (m % M);

    // Convert and push new value
    old = fifo[m];
    subMEM::mcs_convert(&mcsc[ns], &fifo[m], &smuCFG->p1);

    for (ch=0; ch<smu_Nch; ch++){
        // Calculate input difference
        dx = (fifo[m].ch[ch] - old.ch[ch])/M;

        // Modulated Sliding DFT (mSDFT)
        for(K=0;K<Kn;K++){
            // Shift twiddle phase
            k0 = (( m*(K+1))%M);
            k1 = ((k0+(K+1))%M);

            // Demodulate input difference
            dX0r[K] = wMr[k0]*dx;
            dX0i[K] = wMi[k0]*dx;

            // Update demodulated phasor
            mX0r[ch][K] = mX0r[ch][K] + dX0r[K];
            mX0i[ch][K] = mX0i[ch][K] + dX0i[K];

            // Update modulated phasor
            mXkr[ch][K] = (mX0r[ch][K]*wMr[k1] + mX0i[ch][K]*wMi[k1]);
            mXki[ch][K] = (mX0i[ch][K]*wMr[k1] - mX0r[ch][K]*wMi[k1]);
        }
    }

    // Increment counters
    nc++; m++;

    // Process output data
    for (ch=0; ch<smu_Nch; ch++){

        // Hanning window in frequency domain
        for(K=1;K<Kn-1;K++)
        {
            mXhr[K] = 0.25*mXkr[ch][K-1] - 0.50*mXkr[ch][K] + 0.25*mXkr[ch][K+1];
            mXhi[K] = 0.25*mXki[ch][K-1] - 0.50*mXki[ch][K] + 0.25*mXki[ch][K+1];
            mXhA[K] = 2*cabs(mXhr[K],mXhi[K]);
        }

        // Interpolated DFT (Ip-DFT)
        epsil = (mXhA[Km+1] > mXhA[Km-1]) ? 1 : -1;
        alpha = mXhA[Km]/mXhA[Km+epsil];
        delta = epsil*(2-alpha)/(1+alpha);

        //phasor[nr].f.ch[ch] = (Km + 1 + delta)*Ns/M;
        phasor[nr].A.ch[ch] = 2*mXhA[Km]*(PI*delta*(1-delta*delta))/sin(PI*delta);
        phasor[nr].P.ch[ch] = atan2(mXhi[Km],mXhr[Km]) + PI*delta;// -wrap(2*PI*f0*ns/Ns);
    }

    // Skip to next iteration
    if (nc<Nc)
        return -1;

    for (ch=0; ch<smu_Nch; ch++){

        //checking for validity of Amplitude value
        if (isnan(phasor[nr].A.ch[ch]))
            phasor[nr].A.ch[ch]=0;

        //checking for validity of Phase value
        if (isnan(phasor[nr].P.ch[ch]))
            phasor[nr].P.ch[ch]=0;
    }

    //instrument error compensation process
    for (ch=0; ch<smu_Nch; ch++)
    {
        if ((nr+1)==Nr)
        {
            //instrument frequency drift compensation
            
            phasor[nr].P.ch[ch] = smuCFG->p2.f_drift.gain*phasor[nr].P.ch[ch] +
                                ((smuCFG->p3.f_drift.gain*phasor[nr].f.ch[ch]+smuCFG->p3.f_drift.offset)-phasor[nr].f.ch[ch])*.2 +
                                  smuCFG->p2.f_drift.offset;
            phasor[nr].f.ch[ch] = smuCFG->p3.f_drift.gain*phasor[nr].f.ch[ch] + 
                                  smuCFG->p3.f_drift.offset;
            phasor[nr].A.ch[ch] = smuCFG->p1.f_drift.gain*phasor[nr].A.ch[ch] +
                                  smuCFG->p1.f_drift.offset;
            phasor[nr].df.ch[ch]= smuCFG->p4.f_drift.gain*phasor[nr].df.ch[ch]+
                                  smuCFG->p4.f_drift.offset;
            
        }
        //isntrument gain and offset compensation
        phasor[nr].P.ch[ch] = smuCFG->p2.ch[ch].gain*phasor[nr].P.ch[ch] +
                              smuCFG->p2.ch[ch].offset;
        phasor[nr].f.ch[ch] = smuCFG->p3.ch[ch].gain*phasor[nr].f.ch[ch] + 
                              smuCFG->p3.ch[ch].offset;
    }

    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            sym_calculation_p[i][j].mag = inv_A[i][j].mag*phasor[nr].A.ch[j];
            sym_calculation_p[i][j].ph  = inv_A[i][j].ph+phasor[nr].P.ch[j];
            sym_calculation_r[i][j].re  = sym_calculation_p[i][j].mag*((float)cos((double)sym_calculation_p[i][j].ph));
            sym_calculation_r[i][j].im  = sym_calculation_p[i][j].mag*((float)sin((double)sym_calculation_p[i][j].ph));
            component_calculation_r.re  = component_calculation_r.re + sym_calculation_r[i][j].re;
            component_calculation_r.im  = component_calculation_r.im + sym_calculation_r[i][j].im;
        }
            component_calculation_p.mag =   (float)sqrt(component_calculation_r.re*component_calculation_r.re+
                                            component_calculation_r.im*component_calculation_r.im);
            component_calculation_p.ph  =   (float)atan2((double)component_calculation_r.im,(double)component_calculation_r.re);
        switch (i)
        {
            case 0:
                sym_v_i[nr].volt.zero = component_calculation_p;
                break;
            case 1:
                sym_v_i[nr].volt.pos = component_calculation_p;
                break;
            default:
                sym_v_i[nr].volt.neg = component_calculation_p;
        }
        component_calculation_r.re = 0;
        component_calculation_r.im  = 0;
    }
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            sym_calculation_p[i][j].mag = inv_A[i][j].mag*phasor[nr].A.ch[j+4];
            sym_calculation_p[i][j].ph  = inv_A[i][j].ph+phasor[nr].P.ch[j+4];
            sym_calculation_r[i][j].re  = sym_calculation_p[i][j].mag*((float)cos((double)sym_calculation_p[i][j].ph));
            sym_calculation_r[i][j].im  = sym_calculation_p[i][j].mag*((float)sin((double)sym_calculation_p[i][j].ph));
            component_calculation_r.re  = component_calculation_r.re + sym_calculation_r[i][j].re;
            component_calculation_r.im  = component_calculation_r.im + sym_calculation_r[i][j].im;
        }
            component_calculation_p.mag =   (float)sqrt(component_calculation_r.re*component_calculation_r.re+
                                            component_calculation_r.im*component_calculation_r.im);
            component_calculation_p.ph  = (float)atan2((double)component_calculation_r.im,(double)component_calculation_r.re);
        switch (i)
        {
            case 0:
                sym_v_i[nr].curr.zero = component_calculation_p;
                break;
            case 1:
                sym_v_i[nr].curr.pos = component_calculation_p;
                break;
            default:
                sym_v_i[nr].curr.neg = component_calculation_p;
        }
        component_calculation_r.re = 0;
        component_calculation_r.im  = 0;
    }
    
    // Process output phasor
    sym_v_i[nr].t_stamp = smuMEM->tref.addMSecs(((nr+1)%Nr)*Tw+(Tw/2)).toMSecsSinceEpoch();
    smuMEM->dsp_pos = nr;

    return (nr++);
}

/**
 * @brief DSP log output data
 *
 */
void lib_log(FILE *fid)
{

}
