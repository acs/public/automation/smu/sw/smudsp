// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "lib_template.h"
#include <math.h>

static subCFG *smuCFG;
static subMEM *smuMEM;

static smu_mcsc_t *mcsc;    // Input data
// static <TYPE> *data;        // Ouput data
static uint32_t ns, nr, nc, Ns, Nc, Nr, Tc;

/// ---------- SMU: DSP virtual methods section ----------

/**
 * @brief DSP allocate memory
 *
 */
int lib_allocate(subCFG *cfg, subMEM *mem)
{
    smuCFG = cfg;
    smuMEM = mem;

    Ns = smuCFG->daq.rate*1000;                         // Sampling rate
    Nr = smuCFG->dsp.rate;                              // Reporting rate
    Nc = Ns/Nr;                                         // MCSamples per frame
    Tc = 1000/Nr;                                       // Time window per frame (ms)

    if (Nr>Ns)
        return -1; // Invalid reporting rate

    if (Ns % Nr)
        return -2; // Invalid rate ratio

    // Allocate data buffer
    smuMEM->dsp_pos = 0;
    //smuMEM->dsp_inc = sizeof(<TYPE>);
    //smuMEM->dsp_data= (quint8*) calloc(Nr, sizeof(<TYPE>));

    // Link buffers
    mcsc = (smu_mcsc_t*)smuMEM->daq_data;
    // data = (<TYPE>*)smuMEM->dsp_data;

    // Init indexes
    nr = 0;
    nc = 0;

    return 0;
}

/**
 * @brief DSP destroy memory
 *
 */
void lib_destroy()
{
    //free(data);
}

/**
 * @brief DSP process multi-channel sample code
 *
 */
int  lib_process()
{
    // Reset cyclic indexes
    ns  = smuMEM->daq_pos;
    nr %= Nr;
    nc %= Nc;

    // Fill output buffer
    // TODO processing from mcsc to data...

    // Increment counters
    nc++;

    // Skip to next iteration
    if (nc<Nc)
        return ((int)(nc-Nc));

    // Tag output data
    smuMEM->dsp_pos = nr;
    qint64 timetag = smuMEM->tref.addMSecs((nr+1)*Tw).toMSecsSinceEpoch(); // Time tag at window beginning

    return (nr++);
}

/**
 * @brief DSP log output data
 *
 */
void lib_log(FILE *fid)
{
    uint32_t n = (smuMEM->dsp_pos+1)%Nr;
    fprintf(fid,"<constant expression>");
}
