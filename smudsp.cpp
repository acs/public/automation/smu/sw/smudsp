// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#include "smudsp.h"
#include "time.h"
#include "math.h"
#include DSP_LIB

int smuDSP_init(subCFG *cfg, subMEM *mem)
{
    if(mem->dsp_data)
        delete[] mem->dsp_data;

    // Allocate libray memory
    return(lib_allocate(cfg,mem));
}

void smuDSP_exit()
{
    // Destroy libray memory
    lib_destroy();
}

int smuDSP_proc()
{
    // Processing method
    return (lib_process());
}

int smuDSP_test()
{
    qDebug() << "Starting DSP test procedure:";
    FILE *fid;
    fid = fopen("test.log","w");
    if (fid==NULL){
        fputs ("File error",stderr);
        exit (1);
    }

    subCFG* cfg = new subCFG();
    subMEM* mem = new subMEM(cfg);
    smuDSP_init(cfg, mem);

    mem->tref = QDateTime::fromTime_t(time(NULL));

    qDebug() << "\tCreating signal";
    uint32_t ch, ns, Ns = 10000, Fs = 10000;
    smu_mcsc_t *mcsc = (smu_mcsc_t*)mem->daq_data;
    for (ns=0;ns<Ns;ns++)
        for (ch=0;ch<smu_Nch;ch++)
            if (ns<Ns/2)
                mcsc[ns].ch[ch] = 16383*cos(2*3.141592653589793*50.0001F*ns/Fs);
            else
                mcsc[ns].ch[ch] = 32767*cos(2*3.141592653589793*50.0001F*ns/Fs);

    qDebug() << "\tProcessing signal";
    fprintf(fid,"Test log: %10s\n",DSP_NAME);
    fprintf(fid,"%3s %6s %5s\n","nr","ns","t[us]");
    int nr;
    struct timespec begin, end;
    for (ns=0;ns<Ns;ns++){
        mem->daq_pos = ns;
        clock_gettime(CLOCK_MONOTONIC_RAW, &begin);
        nr = lib_process();
        qDebug() << nr;
        clock_gettime(CLOCK_MONOTONIC_RAW, &end);
//        if(nr>-1){
//            fprintf(fid,"%3d %6d %5.1f\n",nr,ns,(end.tv_nsec - begin.tv_nsec)/1000.0);
            lib_log(fid);
//        }
    }
    qDebug() << "\tWriting log";

    fclose(fid);
    smuDSP_exit();
    qDebug() << "Test done";

    return 0;
}
