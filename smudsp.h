// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#ifndef SMUDSP_H
#define SMUDSP_H

#include "smudsp_global.h"
#include <subtypes.h>
#include <subcfg.h>
#include <submem.h>

extern "C" SMUDSPSHARED_EXPORT int  smuDSP_init(subCFG *cfg, subMEM *mem);
extern "C" SMUDSPSHARED_EXPORT void smuDSP_exit();

extern "C" SMUDSPSHARED_EXPORT int  smuDSP_proc();
extern "C" SMUDSPSHARED_EXPORT int  smuDSP_test();

#endif // SMUDSP_H
