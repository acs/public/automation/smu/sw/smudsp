// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: Apache-2.0

#ifndef SMUDSP_GLOBAL_H
#define SMUDSP_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(SMUDSP_LIBRARY)
#  define SMUDSPSHARED_EXPORT Q_DECL_EXPORT
#else
#  define SMUDSPSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // SMUDSP_GLOBAL_H
